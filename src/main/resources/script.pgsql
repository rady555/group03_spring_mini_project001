create database todolist;

create table if not exists user_tb
(
    user_id       serial4 primary key,
    user_email    varchar(255) not null,
    user_password varchar(255) not null,
    user_role     varchar(50)  not null
);

create table if not exists category_tb
(
    category_id   serial4 primary key,
    category_name varchar(255) not null,
    category_date timestamp default now(),
    user_id       int4 references user_tb ON UPDATE CASCADE ON DELETE RESTRICT
);

create table if not exists task_tb
(
    task_id          serial4 primary key,
    task_name        varchar(255) not null,
    task_description text         not null,
    task_date        timestamp default now(),
    task_status      varchar(50)  not null,
    user_id          int4 references user_tb ON UPDATE CASCADE ON DELETE RESTRICT,
    category_id      int4 references category_tb ON UPDATE CASCADE ON DELETE RESTRICT
);



CREATE OR REPLACE FUNCTION getRowsByPageNumberAndSize(
    PageNumber INTEGER = NULL,
    PageSize INTEGER = NULL,
    UserId INTEGER = NULL,
    Ascending BOOLEAN = FALSE,
    Descending BOOLEAN = FALSE
)
    RETURNS SETOF public.task_tb
AS
$$
DECLARE
    PageOffset INTEGER := 0;
BEGIN
    PageOffset := ((PageNumber - 1) * PageSize);
    RETURN
        QUERY
        SELECT *
        FROM public.task_tb
        where user_id = UserId
        ORDER BY
            case when Ascending then task_id end,
            case when Descending then task_id end desc
        LIMIT PageSize OFFSET PageOffset;
END;

$$ LANGUAGE plpgsql;