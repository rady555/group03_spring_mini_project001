package com.example.todolist.controller;

import com.example.todolist.model.Entity.Task;
import com.example.todolist.model.request.TaskRequest;
import com.example.todolist.model.response.ResponseMessages;
import com.example.todolist.model.response.ResponseMessagesDelete;
import com.example.todolist.model.response.TaskResponse;
import com.example.todolist.service.TaskService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:3002/")
//@CrossOrigin("http://localhost:3001/")
//@CrossOrigin("http://localhost:3000/")
@RestController
@SecurityRequirement(name = "bearerAuth")
@RequestMapping("/api/v1/tasks")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping
    public ResponseEntity<?> getAllTasks() {
        ResponseMessages<?> tasks = new ResponseMessages<>(
                taskService.getAllTasks(),
                true
        );
        return ResponseEntity.ok(tasks);
    }

    @PostMapping
    public ResponseEntity<?> addTask(@RequestBody TaskRequest taskRequest) {
        ResponseMessages<?> task = new ResponseMessages<>(
                taskService.addTask(taskRequest),
                true
        );
        return ResponseEntity.ok(task);

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTaskById(@PathVariable Integer id) {
        Task foundTask = taskService.getTaskById(id);
        if(foundTask == null){
            return ResponseEntity.notFound().build();
        }
        ResponseMessages<?> task = new ResponseMessages<>(
                taskService.getTaskById(id),
                true
        );
        return ResponseEntity.ok(task);
    }

    @GetMapping("/{id}/users")
    public ResponseEntity<?> getTaskByIdWithCurrentUser(@PathVariable Integer id) {
        ResponseMessages<?> task = new ResponseMessages<>(
                taskService.getTaskByIdWithCurrentUser(id),
                true
        );
        return ResponseEntity.ok(task);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllTasksByCurrentUser(@RequestParam(required = false, defaultValue = "false") Boolean asc, @RequestParam(required = false, defaultValue = "false") Boolean desc, @RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        ResponseMessages<?> tasks = new ResponseMessages<>(
                taskService.getAllTasksByCurrentUser(asc, desc, pageNumber, pageSize),
                true
        );
        return ResponseEntity.ok(tasks);
    }

    @GetMapping("/status/users")
    public ResponseEntity<?> getTaskByStatus(@RequestParam String status) {
        ResponseMessages<?> tasks = new ResponseMessages<>(
                taskService.getTaskByStatus(status),
                true
        );
        return ResponseEntity.ok(tasks);
    }

    @PutMapping("/{id}/users")
    public ResponseEntity<?> updateTaskByCurrentUserById(@PathVariable Integer id, @RequestBody TaskRequest taskRequest) {
        Task foundTask = taskService.getTaskById(id);
        if (foundTask == null) {
            return ResponseEntity.notFound().build();
        }
        ResponseMessages<?> tasks = new ResponseMessages<>(
                taskService.updateTaskByCurrentUserById(id, taskRequest),
                true
        );
        return ResponseEntity.ok(tasks);
    }

    @DeleteMapping("/{id}/users")
    public ResponseEntity<?> deleteTaskByCurrentUser(@PathVariable Integer id) {
        Task task = taskService.getTaskById(id);
        if (task == null) {
            return ResponseEntity.notFound().build();
        }
        taskService.deleteTaskByCurrentUser(id);
        ResponseMessagesDelete message = new ResponseMessagesDelete("Delete ID: " + id + " Successfully", true);
        return ResponseEntity.ok(message);
    }

    @GetMapping("/getTaskName/{name}")
    public Integer getIdByName(@PathVariable String name) {
        return taskService.getIdByName(name);
    }


}
