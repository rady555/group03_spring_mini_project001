package com.example.todolist.controller;
import com.example.todolist.JWT.JwtTokenUtils;
import com.example.todolist.JWT.AuthenticationRequest;
import com.example.todolist.JWT.AuthenticationResponse;
import com.example.todolist.model.request.MyUserRequest;
import com.example.todolist.model.response.MyUserDTO;
import com.example.todolist.model.response.ResponseMessages;
import com.example.todolist.service.MyUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/auth")
@SecurityRequirement(name = "bearerAuth")
@CrossOrigin("http://localhost:3002/")
//@CrossOrigin("http://localhost:3001/")
//@CrossOrigin("http://localhost:3000/")
public class AuthController {

    private final MyUserService myUserService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtils jwtTokenUtils;

    public AuthController(MyUserService myUserService, AuthenticationManager authenticationManager, JwtTokenUtils jwtTokenUtils) {
        this.myUserService = myUserService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtils = jwtTokenUtils;
    }
    //Register
    @PostMapping("/register")
    @Operation(summary = "register")
    public ResponseEntity<ResponseMessages<MyUserDTO>> register(@RequestBody MyUserRequest myUserRequest){
        MyUserDTO myUserDTO = myUserService.insertUser(myUserRequest);
        ResponseMessages responseMessages = new ResponseMessages();
        responseMessages.setPayload(myUserDTO);
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }
    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        final UserDetails userDetails = myUserService
                .loadUserByUsername(authenticationRequest.getEmail());
        final String token = jwtTokenUtils.generateToken(userDetails);

        String loginUsername = authenticationRequest.getEmail();
        String eqaulsUsername = myUserService.getUserName(loginUsername);

        if(loginUsername.equals(eqaulsUsername)){
            Integer userId = myUserService.getUserId(eqaulsUsername);
            authenticationResponse.setUserId(userId);
        }
        authenticationResponse.setToken(token);
        authenticationResponse.setEmail(authenticationRequest.getEmail());
        ResponseMessages responseMessages = new ResponseMessages();
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setPayload(authenticationResponse);
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }

    @GetMapping("/getUsername")
    public List<String> getUsername(){

        return myUserService.getUsername();
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
