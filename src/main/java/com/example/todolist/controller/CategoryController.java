package com.example.todolist.controller;
import com.example.todolist.model.Entity.Category;
import com.example.todolist.model.request.CategoryRequest;
import com.example.todolist.model.response.ResponseMessages;
import com.example.todolist.model.response.ResponseMessagesDelete;
import com.example.todolist.service.CategoriesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/categories")
@SecurityRequirement(name = "bearerAuth")
@CrossOrigin("http://localhost:3002/")
//@CrossOrigin("http://localhost:3001/")
//@CrossOrigin("http://localhost:3000/")
public class CategoryController {

    private final CategoriesService categoriesService;

    public CategoryController(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }
    //Add Categories
    @PostMapping("/")
    public ResponseEntity<ResponseMessages<CategoryRequest>> addNewCategory(@RequestBody CategoryRequest categoryRequest){
        Integer currentId = categoriesService.getCurrentId(getUsernameOfCurrentUser());
        categoryRequest.setUserId(currentId);
        Category category = categoriesService.addNewCategories(categoryRequest);
        ResponseMessages responseMessages = new ResponseMessages();
        responseMessages.setPayload(category);
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }

    String getUsernameOfCurrentUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return userDetails.getUsername();
    }

    //Get All Categories
    @GetMapping("/allCategories")
    public ResponseEntity<ResponseMessages<ArrayList<Category>>> getAllCategories(){
        ResponseMessages responseMessages = new ResponseMessages();
        responseMessages.setPayload(categoriesService.getAll());
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }

    //Get All from CurrentUser
    @GetMapping("/fromCurrentUser")
    public ResponseEntity<ResponseMessages<ArrayList<Category>>> getAllfromCurrentUser(){
        ResponseMessages responseMessages = new ResponseMessages();
        Integer currentId = categoriesService.getCurrentId(getUsernameOfCurrentUser());
        responseMessages.setPayload(categoriesService.getAllFromCurrentUser(currentId));
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }
    //Get By ID
    @GetMapping("/getById/{id}")
    public ResponseEntity<ResponseMessages<ArrayList<Category>>> getByIDCategories(@PathVariable Integer id){
        ResponseMessages responseMessages = new ResponseMessages();
        responseMessages.setPayload(categoriesService.getById(id));
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }

    //GetByID from CurrentUser
    @GetMapping("/getIdCurrentUser")
    public ResponseEntity<?> getIdCurrentUser (@RequestParam(defaultValue = "false") Boolean asc,
                                               @RequestParam(defaultValue = "false") Boolean desc,
                                               @RequestParam(defaultValue = "1") Integer pages,
                                               @RequestParam(defaultValue = "10") Integer size
                                               ){
        Integer currentId = categoriesService.getCurrentId(getUsernameOfCurrentUser());
        List<Category> category = categoriesService.getIdCurrentUser(asc,desc,pages,size,currentId);
        return ResponseEntity.ok().body(category);
    }
    //Update Categories by ID
    @PutMapping("/update/{id}")
    @Operation(summary = "Update By ID")
    @ResponseBody
    public ResponseEntity<ResponseMessages<ArrayList<Category>>> UpdateID(@PathVariable Integer id,
                                                                       @RequestBody CategoryRequest categoryRequest) {
        ResponseMessages responseMessages = new ResponseMessages<>();
        if (categoriesService.getById(id) == null) {
            return ResponseEntity.notFound().build();
        }
        responseMessages.setPayload(categoriesService.UpdateData(id, categoryRequest));
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }
    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete By ID")
    public ResponseEntity<ResponseMessagesDelete> deleteById(@PathVariable Integer id) {
        ResponseMessagesDelete responseMessages = new ResponseMessagesDelete();
        Integer currentId = categoriesService.getCurrentId(getUsernameOfCurrentUser());
        if (currentId == null) {
            return ResponseEntity.notFound().build();
        }
        categoriesService.deleteId(id, currentId);
        responseMessages.setPayload("Delete ID: "+id+" Successfully");
        responseMessages.setDate(LocalDateTime.now());
        responseMessages.setStatus(true);
        return ResponseEntity.ok().body(responseMessages);
    }
}
