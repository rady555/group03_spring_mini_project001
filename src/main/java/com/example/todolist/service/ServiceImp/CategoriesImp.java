package com.example.todolist.service.ServiceImp;
import com.example.todolist.exception.NullExceptionClass;
import com.example.todolist.model.Entity.Category;
import com.example.todolist.model.request.CategoryRequest;
import com.example.todolist.repository.CategoriesRepository;
import com.example.todolist.service.CategoriesService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CategoriesImp implements CategoriesService {
    private final CategoriesRepository categoriesRepository;

    public CategoriesImp(CategoriesRepository categoriesRepository) {
        this.categoriesRepository = categoriesRepository;
    }
    @Override
    public Category addNewCategories(CategoryRequest categoryRequest) {
        if(categoryRequest.getCategoryName().isBlank()) {
            throw new NullExceptionClass("Field NAME can not be empty", "Category");
        }
        categoryRequest.setCategoryName(categoryRequest.getCategoryName().trim());
        return categoriesRepository.addNewCategories(categoryRequest);
    }

    @Override
    public Integer getCurrentId(String usernameOfCurrentUser) {
        return categoriesRepository.getCurrentId(usernameOfCurrentUser);
    }

    @Override
    public List<Category> getAll() {
        return categoriesRepository.getAll();
    }

    @Override
    public Category getById(Integer id) {
        Category category = categoriesRepository.getById(id);
        if (category == null) {
            throw new NullExceptionClass("This category ID is not found", "Category");
        }
        return category;
    }

    @Override
    public Category getByIdForCurrentUser(Integer id, Integer currentId) {
        Category byIdForCurrentUser = categoriesRepository.getByIdForCurrentUser(id, currentId);
        if (byIdForCurrentUser == null) {
            throw new NullExceptionClass("This category ID is not found", "Category");
        }
        return byIdForCurrentUser;
    }

    @Override
    public Category UpdateData(Integer id, CategoryRequest categoryRequest) {
        if (categoryRequest.getCategoryName().isBlank()) {
            throw new NullExceptionClass("Field NAME can not be empty", "Category");
        }
        categoryRequest.setCategoryName(categoryRequest.getCategoryName().trim());
        return categoriesRepository.updateById(id, categoryRequest);
    }

    @Override
    public Category deleteId(Integer id, Integer currentId) {
        Integer findId = categoriesRepository.getById(id).getUserId();
        if (!Objects.equals(currentId, findId)) {
            throw new NullExceptionClass("This category can not be deleted, since you're not the owner", "Category");
        }
        return categoriesRepository.deleteId(id, currentId);
    }

    @Override
    public List<Category> getAllFromCurrentUser(Integer currentId) {
        return categoriesRepository.getAllFromCurrentUser(currentId);
    }

    @Override
    public List<Category> getIdCurrentUser(Boolean asc, Boolean desc, Integer pages, Integer size, Integer currentId) {
        return categoriesRepository.getIdCurrentUser(asc,desc,pages,size,currentId);
    }

}
