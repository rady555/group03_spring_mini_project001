package com.example.todolist.service.ServiceImp;

import com.example.todolist.exception.NotFoundExceptionClass;
import com.example.todolist.exception.NullExceptionClass;
import com.example.todolist.model.Entity.Task;
import com.example.todolist.model.request.TaskRequest;
import com.example.todolist.repository.TaskRepository;
import com.example.todolist.service.MyUserService;
import com.example.todolist.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final MyUserService userService;

    @Override
    public List<Task> getAllTasks() {
        return taskRepository.getAllTasks();
    }

    @Override
    public Task addTask(TaskRequest taskRequest) {
        Integer userId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());
        if (taskRequest.getTaskName().isBlank() ||
                taskRequest.getTaskDescription().isBlank() ||
                taskRequest.getTaskDate().toString().isBlank()) {
            throw new NullExceptionClass("Field NAME can not be empty", "Category");
        }
        if (!taskRequest.getStatus().trim().equalsIgnoreCase("is_cancelled") &&
                        !taskRequest.getStatus().trim().equalsIgnoreCase("is_completed") &&
                        !taskRequest.getStatus().trim().equalsIgnoreCase("is_in_review") &&
                        !taskRequest.getStatus().trim().equalsIgnoreCase("is_in_progress")

        ) {
            throw new NotFoundExceptionClass("Please input one of (is_cancelled, is_completed, is_in_progress, is_in_review)", "Task");
        }
        TaskRequest task = new TaskRequest(
                taskRequest.getTaskName().trim(),
                taskRequest.getTaskDescription().trim(),
                taskRequest.getStatus().trim(),
                userId,
                taskRequest.getCategoryId()
        );
        return taskRepository.addTask(task);
    }


    @Override
    public Task getTaskByIdWithCurrentUser(Integer id) {
        Integer userId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());
        Integer taskUserId = taskRepository.getTaskById(id).getUserId();
        if (Objects.equals(userId, taskUserId)) {
            return taskRepository.getTaskById(id);
        }
        throw new NullExceptionClass("This task ID is not found", "Task");

    }

    @Override
    public Task getTaskById(Integer id) {
        return taskRepository.getTaskById(id);
    }

    @Override
    public List<Task> getAllTasksByCurrentUser(Boolean asc, Boolean desc, Integer pageNumber, Integer pageSize) {
        Integer userId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());
        return taskRepository.getAllTasksFromCurrentUsers(asc, desc, pageNumber, pageSize, userId);
    }

    @Override
    public Task updateTaskByCurrentUserById(Integer id, TaskRequest taskRequest) {
        Task foundTask = getTaskByIdWithCurrentUser(id);
        TaskRequest task = TaskRequest.builder()
                .taskName(taskRequest.getTaskName().trim())
                .taskDescription(taskRequest.getTaskDescription().trim())
                .status(taskRequest.getStatus().trim())
                .categoryId(taskRequest.getCategoryId())
                .build();
        if (foundTask != null) {
            taskRepository.updateTaskByCurrentUserById(id, task);
            return taskRepository.getTaskById(id);

        }
        throw new NullExceptionClass("This task ID is not found", "Task");


    }

    @Override
    public void deleteTaskByCurrentUser(Integer id) {
        Integer currentUserId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());
        Integer deleteUserId = taskRepository.getTaskById(id).getUserId();
        if (!Objects.equals(currentUserId, deleteUserId)) {
            throw new NullExceptionClass("This task can not be deleted, since you're not the owner", "Task");

        }

        taskRepository.deleteTaskByCurrentUser(id);
    }

    @Override
    public List<Task> getTaskByStatus(String status) {
        Integer userId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());
        return taskRepository.getAllTasks().stream().filter(task -> Objects.equals(task.getUserId(), userId))
                .filter(task -> Objects.equals(task.getStatus(), status))
                .toList();
    }

    @Override
    public Integer getIdByName(String name) {
        return taskRepository.getIdByName(name);
    }
}
