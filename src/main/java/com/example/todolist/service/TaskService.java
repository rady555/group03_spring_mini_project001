package com.example.todolist.service;

import com.example.todolist.model.Entity.Task;
import com.example.todolist.model.request.TaskRequest;
import com.example.todolist.model.response.TaskResponse;
import org.springframework.data.relational.core.sql.In;

import java.util.List;

public interface TaskService {
    List<Task> getAllTasks();

    Task addTask(TaskRequest taskRequest);

    Task getTaskByIdWithCurrentUser(Integer id);

    Task getTaskById(Integer id);

    List<Task> getAllTasksByCurrentUser(Boolean asc, Boolean desc, Integer pageNumber, Integer pageSize);

    Task updateTaskByCurrentUserById(Integer id, TaskRequest taskRequest);

    void deleteTaskByCurrentUser(Integer id);

    List<Task> getTaskByStatus(String status);

    Integer getIdByName(String name);
}
