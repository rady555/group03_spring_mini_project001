package com.example.todolist.service;

import com.example.todolist.model.Entity.Category;
import com.example.todolist.model.request.CategoryRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoriesService {

    Category addNewCategories(CategoryRequest categoryRequest);

    Integer getCurrentId(String usernameOfCurrentUser);

    List<Category> getAll();

    Category getById(Integer id);

    Category getByIdForCurrentUser(Integer id, Integer currentId);

    Category UpdateData(Integer id, CategoryRequest categoryRequest);

    Category deleteId(Integer id, Integer currentId);

    List<Category> getAllFromCurrentUser(Integer currentId);

    List<Category> getIdCurrentUser(Boolean asc, Boolean desc, Integer pages, Integer size, Integer currentId);

}
