package com.example.todolist.service;

import com.example.todolist.model.Entity.MyUser;
import com.example.todolist.model.Entity.User;
import com.example.todolist.model.request.MyUserRequest;
import com.example.todolist.model.response.MyUserDTO;
import com.example.todolist.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyUserService implements UserDetailsService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public MyUserService(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MyUser myUser = userRepository.findUserByEmail(username);
        if(myUser == null){
            throw new UsernameNotFoundException("User Not Found");
        }
        return myUser;
    }

    public MyUserDTO insertUser(MyUserRequest myUserRequest) {
        myUserRequest.setPassword(passwordEncoder.encode(myUserRequest.getPassword()));
        MyUser myUser = userRepository.insertUser(myUserRequest);
        MyUserDTO myUserDTO = new MyUserDTO();
        myUserDTO.setUserId(myUser.getUserId());
        myUserDTO.setEmail(myUser.getUserEmail());
        myUserDTO.setUserRole(myUser.getUserRole());
        return myUserDTO;
    }

    public Integer getUserId(String usernameOfCurrentUser) {
        return userRepository.getId(usernameOfCurrentUser);
    }

    public String getUserName(String loginUsername) {
        String username = userRepository.getUserName(loginUsername);
        if(username == null){
            throw new UsernameNotFoundException("User Not Found");
        }
        return username;
    }
    public Integer currentUserId(Authentication authentication) {
        MyUser user = userRepository.findUserByEmail(authentication.getName());
        return user.getUserId();
    }

    public List<String> getUsername() {
        return userRepository.getEmail();
    }
}
