package com.example.todolist.repository;
import com.example.todolist.model.Entity.Category;
import com.example.todolist.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CategoriesRepository{
    @Select("""
            insert into category_tb (category_name, category_date, user_id)
            values (#{c.categoryName}, #{c.categoryDate}, #{c.userId})
            RETURNING *
            """)
    @Results(id = "categoriesMap", value = {
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "categoryName", column = "category_name"),
            @Result(property = "categoryDate", column = "category_date"),
            @Result(property = "userId", column = "user_id"),
    })
    Category addNewCategories(@Param("c") CategoryRequest categoryRequest);
    @Select("""
            select user_id from user_tb where user_email = #{usernameOfCurrentUser}
            """)
    Integer getCurrentId(String usernameOfCurrentUser);

    @Select("""
            Select * From category_tb
            """)
    @ResultMap("categoriesMap")
    List<Category> getAll();

    @Select("""
            Select * From category_tb where category_id = #{id}
            """)
    @ResultMap("categoriesMap")
    Category getById(Integer id);

    @Select("""
            Select * from category_tb where category_id = #{id} AND user_id = #{currentId}
            """)
    @ResultMap("categoriesMap")
    Category getByIdForCurrentUser(Integer id, Integer currentId);

    @Select("""
            UPDATE category_tb SET category_name = #{a.categoryName}
            WHERE category_id = #{id}
                RETURNING *
            """)
    @ResultMap("categoriesMap")
    Category updateById(Integer id, @Param("a") CategoryRequest categoryRequest);

    @Select("""
            DELETE from category_tb where user_id = #{currentId} and category_id = #{id}
            """)
    @ResultMap("categoriesMap")
    Category deleteId(Integer id, Integer currentId);

    @Select("""
            Select * from category_tb where user_id = #{currentId}
            """)
    @ResultMap("categoriesMap")
    List<Category> getAllFromCurrentUser(Integer currentId);


    @Select("""
                SELECT * FROM category_tb
                WHERE user_id = #{currentId}
                ORDER BY
                     CASE WHEN #{asc} THEN category_id END ASC,
                     CASE WHEN #{desc} THEN category_id END DESC
                LIMIT #{size} OFFSET #{pages} * ( #{pages} - 1 )
            """)
    @ResultMap("categoriesMap")
    List<Category>getIdCurrentUser(Boolean asc, Boolean desc, Integer pages, Integer size, Integer currentId);

    @Select("""
            Select 
            """)
    Integer findId(Integer currentId);
}



