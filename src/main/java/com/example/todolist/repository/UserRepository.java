package com.example.todolist.repository;

import com.example.todolist.model.Entity.MyUser;
import com.example.todolist.model.request.MyUserRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserRepository {
    @Select("""
            insert into user_tb (user_email,user_password,user_role) 
            values (#{u.Email},#{u.Password}, #{u.userRole})
            returning *
            """)
    @Results(id = "userMapping", value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "userEmail", column = "user_email"),
            @Result(property = "userPassword", column = "user_password"),
            @Result(property = "userRole", column = "user_role"),
    })
    MyUser insertUser(@Param("u") MyUserRequest myUserRequest);

    @Select("""
            SELECT * FROM user_tb
            WHERE user_email = #{username} \s
            """)
    @ResultMap("userMapping")
    MyUser findUserByEmail(String username);

    @Select("""
            SELECT user_id FROM user_tb WHERE user_email = #{usernameOfCurrentUser}
            """)
    Integer getId(String usernameOfCurrentUser);

    @Select("""
            SELECT user_email FROM user_tb WHERE user_email = #{loginUsername}
            """)
    String getUserName(String loginUsername);

    
    @Select("""
            Select user_email from user_tb
            """)
    List<String> getEmail();
}
