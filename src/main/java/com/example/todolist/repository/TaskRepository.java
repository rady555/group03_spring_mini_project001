package com.example.todolist.repository;

import com.example.todolist.model.Entity.Task;
import com.example.todolist.model.request.TaskRequest;
import com.example.todolist.model.response.TaskResponse;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TaskRepository {

    @Select("""
            select * from task_tb
            """)
    @Results(id = "taskMap", value = {
            @Result(property = "taskId", column = "task_id"),
            @Result(property = "taskName", column = "task_name"),
            @Result(property = "taskDescription", column = "task_description"),
            @Result(property = "taskDate", column = "task_date"),
            @Result(property = "status", column = "task_status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "categoryId", column = "category_id"),
    })
    List<Task> getAllTasks();

    @Select("""
            select *
            from getRowsByPageNumberAndSize(#{pageNumber}, ${pageSize},${userId},#{asc},#{desc});
            """)
    @ResultMap(value = "taskMap")
    List<Task> getAllTasksFromCurrentUsers(Boolean asc, Boolean desc, Integer pageNumber, Integer pageSize, Integer userId);

    @Select("""
            insert into task_tb (task_name, task_description, task_date, task_status, user_id, category_id)
                                values (#{taskName}, #{taskDescription}, #{taskDate}, #{status}, #{userId}, #{categoryId})
                                returning *;
            """)
    @ResultMap(value = "taskMap")
    Task addTask(TaskRequest taskRequest);


    @Select("""
            select * from task_tb where task_id = #{id};
                        
                        """)
    @ResultMap(value = "taskMap")
    Task getTaskById(Integer id);

    @Update("""
            update task_tb set task_name = #{tk.taskName},task_description =  #{tk.taskDescription}, task_date = #{tk.taskDate},
            task_status =  #{tk.status}, category_id = #{tk.categoryId}
            where task_id = #{taskId}
                        
                        """)
//    @ResultMap(value = "taskMap")

    void updateTaskByCurrentUserById(Integer taskId, @Param("tk") TaskRequest taskRequest);

    @Delete("""
            delete from task_tb where task_id = #{id}
            """)
    void deleteTaskByCurrentUser(Integer id);

    @Select("""
            SELECT category_id FROM category_tb WHERE category_name = #{name}
            """)
    Integer getIdByName( String name);
}
