package com.example.todolist.model.Entity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MyUser implements UserDetails{
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Integer userId;
    String userEmail;
    String userPassword;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    String userRole = "ROLE_USER";

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(this.userRole.toUpperCase());
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return userPassword;
    }
    @Override
    public String getUsername() {
        return userEmail;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
