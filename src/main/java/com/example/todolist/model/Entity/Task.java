package com.example.todolist.model.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    private Integer taskId;
    private String taskName;
    private String taskDescription;
    private Timestamp taskDate;
    private String status;
    private Integer userId;
    private Integer categoryId;
}
