package com.example.todolist.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskRequest {
    private String taskName;
    private String taskDescription;
    private LocalDateTime taskDate = LocalDateTime.now();
    private String status;
    @JsonIgnore
    private Integer userId;
    private Integer categoryId;



    public TaskRequest(String taskName, String taskDescription, String status, Integer userId,  Integer categoryId) {
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.taskDate = LocalDateTime.now();
        this.status = status;
        this.userId = userId;
        this.categoryId = categoryId;
    }
}
