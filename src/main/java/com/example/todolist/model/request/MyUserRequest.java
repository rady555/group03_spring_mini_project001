package com.example.todolist.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyUserRequest {
    String Email;
    String Password;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    String userRole = "ROLE_USER";
}
