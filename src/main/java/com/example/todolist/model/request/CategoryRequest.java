package com.example.todolist.model.request;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryRequest {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Integer categoryId;
    String categoryName;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    LocalDateTime categoryDate = LocalDateTime.now();
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Integer userId;
}
