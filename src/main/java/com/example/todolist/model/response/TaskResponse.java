package com.example.todolist.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;
@Data
@Builder
@AllArgsConstructor
public class TaskResponse {
    private String taskName;
    private String taskDescription;
    private Timestamp taskDate;
    private String status;
    private Integer userId;
    private Integer categoryId;
}
