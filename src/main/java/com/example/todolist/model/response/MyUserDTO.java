package com.example.todolist.model.response;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MyUserDTO{
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Integer userId;
    String Email;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    String userRole = "ROLE_USER";
}
