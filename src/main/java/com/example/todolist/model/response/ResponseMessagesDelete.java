package com.example.todolist.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessagesDelete{
    String payload;
    LocalDateTime date;
    Boolean status;

    public ResponseMessagesDelete(String payload, Boolean status) {
        this.payload = payload;
        this.date = LocalDateTime.now();
        this.status = status;
    }
}
