package com.example.todolist.exception;

public class NotFoundExceptionClass extends RuntimeException {
    private final String title;

    public NotFoundExceptionClass(String message, String title) {
        super(message);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
